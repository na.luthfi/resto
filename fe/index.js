async function fetchMenu() {
    const menuRequest = await fetch('http://localhost:4000/graphql', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(`{
            query {
                Foods{
                  id
                }
              }
        }`)
    })

    const menuRequestResponse = await menuRequest.json()
    console.log(menuRequestResponse)
}

window.onload = () => {
    fetchMenu()
}
