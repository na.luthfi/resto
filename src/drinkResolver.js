let database = require('./database')

const drinkResolver = {
    Drinks: () => {
        return database.drink
    },
    Drink: ({ id }) => {
        const findDrinkIndex = database.drink.findIndex(drink => drink.id == id)
        if(findDrinkIndex !== -1) {
            return database.drink[findDrinkIndex]
        }
        else {
            return null
        }
    }
}

module.exports = drinkResolver