let database = require('./database')
let drinkResolver = require('./drinkResolver')

const resolver = {
    Foods: () => {
        console.log(database)
        return database.food
    },
    Food: (payloads) => {
        console.log({payloads})
        const findFoodIndex = database.food.findIndex(food => food.id == id)
        if(findFoodIndex !== -1) {
            return database.food[findfoodIndex]
        }
        else {
            return null
        }
    },
    ...drinkResolver,
    Menus: () => {
        console.log(database)
        return {
            food: database.food,
            drink: database.drink
        }
    }
}

module.exports = resolver