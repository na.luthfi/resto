const express = require('express')
const grahpQLHTTP = require('express-graphql')
const path = require('path')
const schema = require('./schema')
const resolver = require('./resolver')

const app = express()

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/../fe/index.html'))
})

app.use('/graphi', grahpQLHTTP({
    schema,
    rootValue: resolver,
    graphiql: true
}))

app.use('/graphql', grahpQLHTTP({
    schema,
    rootValue: resolver,
    graphiql: false
}))

app.listen(4000, () => console.log('listening on port 4000'))