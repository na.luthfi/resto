const { buildSchema } = require('graphql')

const schema = buildSchema(`
    type Query {
        Foods: [FoodType]
        Food(id: ID): FoodType
        Drinks: [DrinkType]
        Drink(id: ID): DrinkType
        Menus: Menu
    }

    type FoodType {
        id: ID
        name: String
        price: Int
        spicyLevel: Int
    }

    type Menu {
        food: [FoodType]
        drink: [DrinkType]
    }

    enum DrinkSize {
        SMALL
        MEDIUM
        LARGE
    }

    type DrinkType {
        id: ID
        name: String
        price: Int
        drinkSize: DrinkSize
    }
`)

module.exports = schema