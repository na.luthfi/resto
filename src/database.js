let database = {
    food: [
        {
            id: 1,
            name: 'Bakso',
            price: 20000,
            spicyLevel: 1
        },
        {
            id: 2,
            name: 'Sate',
            price: 30000,
            spicyLevel: 2
        },
        {
            id: 3,
            name: 'Pecel',
            price: 25000,
            spicyLevel: 3
        },
        {
            id: 4,
            name: 'Mie Ayam',
            price: 15000,
            spicyLevel: 4
        },
        {
            id: 5,
            name: 'Nasi Goreng',
            price: 15000,
            spicyLevel: 5
        },
        {
            id: 6,
            name: 'Nasi Uduk',
            price: 15000,
            spicyLevel: 6
        },
        {
            id: 7,
            name: 'Lontong Sayur',
            price: 15000,
            spicyLevel: 7
        }
    ],
    drink: [
        {
            id: 1,
            name: 'Teh',
            price: 4000,
            drinkSize: 'SMALL'
        },
        {
            id: 2,
            name: 'Jus Jeruk',
            price: 10000,
            drinkSize: 'MEDIUM'
        },
        {
            id: 3,
            name: 'Soda Gembira',
            price: 15000,
            drinkSize: 'LARGE'
        }
    ]
}

module.exports = database